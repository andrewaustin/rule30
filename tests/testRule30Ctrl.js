'use strict';

describe('controller', function() {
    var scope;

    beforeEach(module('rule30.controllers'));

    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        $controller('Rule30Ctrl', {
            $scope: scope
        });

    }));

    describe('do_rules', function() {
        it('should be a function',function() {
            expect(typeof scope.do_rules).toEqual('function');
        });

        it('[0, 0, 0] -> 0',function() {
            expect(scope.do_rules([0,0,0])).toEqual(0);
        });

        it('[0, 0, 1] -> 1',function() {
            expect(scope.do_rules([0,0,1])).toEqual(1);
        });

        it('[0, 1, 0] -> 1',function() {
            expect(scope.do_rules([0,1,0])).toEqual(1);
        });

        it('[0, 1, 1] -> 1',function() {
            expect(scope.do_rules([0,1,1])).toEqual(1);
        });

        it('[1, 0, 0] -> 1',function() {
            expect(scope.do_rules([1,0,0])).toEqual(1);
        });

        it('[1, 0, 1] -> 0',function() {
            expect(scope.do_rules([1,0,1])).toEqual(0);
        });

        it('[1, 1, 0] -> 0',function() {
            expect(scope.do_rules([1,1,0])).toEqual(0);
        });

        it('[1, 1, 1] -> 0',function() {
            expect(scope.do_rules([1,1,1])).toEqual(0);
        });
    });

    describe('iteration', function() {
        it('1st iteration produces 2nd iteration', function() {
            expect(scope.iterate([1])).toEqual([1, 1, 1]);
        });

        it('2nd iteration produces 3nd iteration', function() {
            expect(scope.iterate([1, 1, 1])).toEqual([1, 1, 0, 0, 1]);
        });

        it('3rd iteration produces 4th iteration', function() {
            expect(scope.iterate([1, 1, 0, 0, 1])).toEqual([1, 1, 0, 1, 1, 1, 1]);
        });

        it('15th iteration produces 16th iteration', function() {
            var fifteenth = [1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1];
            var sixteenth = [1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1];
            expect(scope.iterate(fifteenth)).toEqual(sixteenth);
        });
    })
});