'use strict';

// Declare app level module which depends on filters, and services

angular.module('rule30App', ['ngRoute', 'rule30.controllers', 'rule30.directives', 'ui.bootstrap'])
    .config(function ($routeProvider, $locationProvider) {

        $routeProvider.
            when('/', {
                templateUrl: 'partials/rule30',
                controller: 'Rule30Ctrl'
            }).
            otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
    });
