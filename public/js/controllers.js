'use strict';

/* Controllers */

angular.module('rule30.controllers', []).
   controller('Rule30Ctrl', function ($scope, $timeout) {

        $scope.iterations = 1;
        $scope.rows = [[1]];
        $scope.timer = undefined;
        $scope.border = false;

        $scope.toggleBorder = function() {
            $scope.border = !$scope.border;
        };

        $scope.do_rules =  function(triplet) {
            if(triplet[0] === 1) {
                // 1, X, X -> ?
                if(triplet[1] === 1) {
                    // 1, 1, X -> 0
                    return 0;
                } else {
                    // 1, 0, X -> ?
                    if(triplet[2] === 1) {
                        // 1, 0, 1 -> 0
                        return 0;
                    } else {
                        // 1, 0, 0 -> 1
                        return 1;
                    }
                }
            } else {
                // 0, X, X -> ?
                if(triplet[1] === 1) {
                    // 0, 1, X -> 1
                    return 1;
                } else {
                    // 0, 0, X -> ?
                    if(triplet[2] === 1) {
                        // 0, 0, 1 -> 1
                        return 1
                    } else {
                        // 0, 0, 0 -> 0
                        return 0
                    }
                }
            }
        };

        $scope.iterate = function(previous_iteration) {
            var result = [],
                i,
                len;

            var imaginary_iteration = previous_iteration.slice(0);

            imaginary_iteration.unshift(0);
            imaginary_iteration.unshift(0);
            imaginary_iteration.push(0);
            imaginary_iteration.push(0);

            for(i = 0, len = imaginary_iteration.length - 2; i < len; i+=1) {
                var left = imaginary_iteration[i] || 0;
                var middle = imaginary_iteration[i+1] || 0;
                var right = imaginary_iteration[i+2] || 0;
                result.push($scope.do_rules([left, middle, right]));
            }

            return result;
        };

        $scope.start = function() {
            if(!$scope.timer) {
                  $scope.timer = $timeout($scope.tick, 1000);
            }
        };

        $scope.tick = function() {
            var i,
                len;

            $scope.iterations += 1;

            var last_row = $scope.rows[$scope.rows.length-1];
            var new_row = $scope.iterate(last_row);

            for(i = 0, len = $scope.rows.length; i < len; i +=1) {
                $scope.rows[i].unshift(0);
                $scope.rows[i].push(0);
            }

            $scope.rows.push(new_row);

            $scope.timer = $timeout($scope.tick, 1000);
        };

        $scope.stop = function() {
            $timeout.cancel($scope.timer);
            $scope.timer = undefined;

        };

        $scope.clear = function() {
            if($scope.timer) {
                $timeout.cancel($scope.timer);
            }
            $scope.timer = undefined;
            $scope.rows = [[1]];
            $scope.iterations = 1;
        };
    });
