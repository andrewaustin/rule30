'use strict';

/* Directives */

angular.module('rule30.directives', []).
    directive('rule30', function () {
        return {
            restrict: 'E',
            templateUrl: 'partials/table'
        };
    });
