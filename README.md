# Rule 30

[SEE IT IN ACTION](http://pacific-tundra-6981.herokuapp.com/)

## Problem

Using the webpage http://mathworld.wolfram.com/Rule30.html as a reference, write a program which computes, and shows,
successive iterations of the rule30 cellular automaton.  There should be a command to start the evolution of the
automaton, and a command to stop the evolution.

## Features

* Makes use of angular-ui/bootstrap for some simple styling
* Clicking start adds a new iteration every second
* Borders around cells can be toggled

## Setup

1. Clone Repo
2. Run ```npm install```

## Running

1. Run ```node app.js```

## Running Tests

1. Run ```./node_modules/karma/bin/karma start karma.conf.js &```
2. Run ```./node_modules/karma/bin/karma run```

## Known Issues

* **Things start to get slow after iteration 150 or so.**
* Using a router and a directive is overkill.
* Only tested in Chrome and Firefox (I don't have IE anywhere handy).
* Probably other inefficiencies (like making a copy with slice(0)).
* No end to end testing.
* No unit test coverage for any of the timer methods (start, stop, clear, tick).
* JS/CSS not minified.

## Citation

** Initial project structure based on https://github.com/eastbayjake/meanseed.**

## License
MIT
